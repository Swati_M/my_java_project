var cars1 = [
    {label:"JAN", value: 0},
    {label:"FEB", value: 0},
    {label:"MAR", value: 0},
    {label:"APR", value: 0},
    {label:"MAY", value: 0},
    {label:"JUN", value: 0},
    {label:"JUL", value: 0},
    {label:"AUG", value: 0} ,
    {label:"SEP", value: 0} ,
    {label:"OCT", value: 0} ,
    {label:"NOV", value: 0} ,
    {label:"DEC", value: 0} 
];

var cars2 = [
    {label:"JUN", value: 25},
    {label:"JAN", value: 25},
    {label:"JUL", value: 0},
    {label:"FEB", value: 25}
];
const cars1IDs = new Set(cars1.map(({ id }) => id));

const combined = [
  ...cars1,
  ...cars2.filter(({ id }) => !cars1IDs.has(id))

];
console.log(combined);
