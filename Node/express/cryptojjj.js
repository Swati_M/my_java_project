
var crypto=require('crypto');
const iv = '12345678';
const key = '123456'.padEnd(24,'0');
const ivHex = Buffer.from(iv, 'utf8');
const keyHex = Buffer.from(key, 'utf8');


const decrypt = (text)=>{
    const cipher = crypto.createDecipheriv('DES-EDE3-CBC', keyHex,ivHex);
    let c = cipher.update(text, 'base64','utf8')
    c += cipher.final('utf8');
    return c;
}

const encrypt = (text)=>{
    const cipher = crypto.createCipheriv('DES-EDE3-CBC', keyHex,ivHex);
    let c = cipher.update(text, 'utf8' ,'base64');
    c+=cipher.final('base64');
    return c;
}

const text = '7LBIMxZKDB0=';
const plaintext = decrypt(text);
console.log(plaintext);//123456

const cipherText = encrypt(plaintext);
console.log(cipherText, text === cipherText);//7LBIMxZKDB0= true